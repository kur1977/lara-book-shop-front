import { createApp, watch } from 'vue'
import './style.css'
import App from './App.vue'
import axios from "axios";
import router from "./router";

// Vuetify
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import { createPinia } from 'pinia'
import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader
Window.axios = axios;

const vuetify = createVuetify({
    components,
    directives,
    icons: {
        defaultSet: 'mdi', // This is already the default value - only for display purposes
    }
})

const pinia = createPinia()

createApp(App).use(router).use(vuetify).use(pinia).mount('#app')

