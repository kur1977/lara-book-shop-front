import {createRouter, createWebHistory} from "vue-router";

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/transactions',
            component: ()=>import('../Pages/Transactions/Index.vue'),
            name:'transactions.index'
        },
        {
            path: '/products',
            component: ()=>import('../Pages/Products/Index.vue'),
            name:'products.index'
        },
        {
            path: '/products/:id',
            component: ()=>import('../Pages/Products/Show.vue'),
            name:'products.show'
        },
        {
            path: '/products/create',
            component: ()=>import('../Pages/Products/Create.vue'),
            name:'products.create'
        },
        {
            path: '/products/edit/:id',
            component: ()=>import('../Pages/Products/Edit.vue'),
            name:'products.edit'
        },
        {
            path: '/login',
            component: ()=>import('../Pages/User/Login.vue'),
            name:'user.login'
        },
        {
            path: '/register',
            component: ()=>import('../Pages/User/Register.vue'),
            name:'user.registration'
        }
    ]
})

router.beforeEach((to, from, next) => {

    const accessToken = localStorage.getItem('access_token')

    if (!accessToken) {
        if (to.name === 'user.login' || to.name === 'user.registration') {
            return next()
        } else {
            return next({
                name: 'user.login'
            })
        }
    }

    if ( (to.name === 'user.login' || to.name === 'user.registration') && accessToken) {
        return next({
            name: 'transactions.index'
        })
    }
    next()
})

export default router