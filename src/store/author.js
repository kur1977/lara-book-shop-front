import { defineStore } from 'pinia'
import shmaxios from "../api/index.js";

export const useAuthorStore = defineStore('author', {
    state: () => ({
        authors: [],
    }),
    getters: {
        getAuthors: (state) => state.authors,
    },
    actions: {
        setAuthors() {
            shmaxios.get('/authors')
            .then(res => {
                this.authors = res.data
            })
        }
    },
})
