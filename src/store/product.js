import { defineStore } from 'pinia'
import shmaxios from "../api/index.js";

export const useProductStore = defineStore('product', {
    state: () => ({
        products:  [],
    }),
    getters: {
        getProducts: (state) => state.products,
    },
    actions: {
        setProducts(filters) {
            shmaxios.get('/products',{
            params: filters
            })
            .then(res => {
                this.products = res.data
            })
        },
    },
})
